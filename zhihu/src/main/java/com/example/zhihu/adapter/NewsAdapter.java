package com.example.zhihu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zhihu.R;
import com.example.zhihu.entity.News;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Oscar on 15/6/11.
 */
public class NewsAdapter extends ArrayAdapter<News> {

    private LayoutInflater mInflater;
    private int mResource;
    private ImageLoader mImageLoader = ImageLoader.getInstance();

    private DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.no_image)
            .showImageOnFail(R.drawable.no_image)
            .showImageForEmptyUri(R.drawable.no_image)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            .build();

    public NewsAdapter(Context context, int resource) {
        super(context, resource);
        this.mInflater = LayoutInflater.from(context);
        this.mResource = resource;
    }

    public NewsAdapter(Context context, int resource, List<News> objects){
        super(context, resource, objects);
        this.mInflater = LayoutInflater.from(context);
        this.mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(this.mResource, null);
            holder = new ViewHolder();
            holder.newsImage = (ImageView) convertView.findViewById(R.id.news_image);
            holder.newsTitle = (TextView) convertView.findViewById(R.id.news_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        News news = getItem(position);
        holder.newsTitle.setText(news.getTitle());
        holder.newsTitle.setTextColor(Color.BLACK);
        mImageLoader.displayImage(news.getImage(), holder.newsImage, options);

        return convertView;
    }

    public void refreshNewsList(List<News> newsList){
        clear();
        addAll(newsList);
        notifyDataSetChanged();
    }

    class ViewHolder{
        ImageView newsImage;
        TextView newsTitle;
    }
}
