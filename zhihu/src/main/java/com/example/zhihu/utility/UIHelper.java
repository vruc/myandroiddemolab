package com.example.zhihu.utility;

import android.content.Context;
import android.content.Intent;

import com.example.zhihu.activity.NewsDetailActivity;
import com.example.zhihu.entity.News;

import java.net.ContentHandler;

/**
 * Created by ChanRuiDa on 15/6/15.
 */
public final class UIHelper {

    public static void startNewsDetail(Context context, News news){
        if(Utility.checkNetworkConnection()){
            Intent i = new Intent(context, NewsDetailActivity.class);
            i.putExtra("news", news);
            i.putExtra("news_id", news.getId());
            context.startActivity(i);
        } else{
            Utility.noNetworkAlert();
        }
    }
}
