package com.example.zhihu.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.example.zhihu.ZhiHuDailyApplication;

/**
 * Created by Oscar on 15/6/10.
 */
public final class Utility {

    public static boolean checkNetworkConnection(Context context){
        ConnectivityManager cm = (ConnectivityManager )context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        return activeNetworkInfo  != null && activeNetworkInfo .isConnected();
    }

    public static boolean checkNetworkConnection(){
        return checkNetworkConnection(ZhiHuDailyApplication.getContext());
    }

    public static void noNetworkAlert(Context context){
        showToast(context, "No Network");
    }

    public static void noNetworkAlert(){
        noNetworkAlert(ZhiHuDailyApplication.getContext());
    }

    public static void showToast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void showToast(String msg){
        showToast(ZhiHuDailyApplication.getContext(), msg);
    }

}
