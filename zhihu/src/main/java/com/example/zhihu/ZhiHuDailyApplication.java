package com.example.zhihu;

import android.app.Application;
import android.content.Context;

import com.example.zhihu.utility.Utility;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 * Created by Oscar on 15/6/10.
 */
public class ZhiHuDailyApplication extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        this.mContext = this;

        initImageLoader(this);
    }

    private static void initImageLoader(Context context){

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .denyCacheImageMultipleSizesInMemory()
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();

        ImageLoader.getInstance().init(config);
    }

    public static Context getContext(){
        return mContext;
    }

}
