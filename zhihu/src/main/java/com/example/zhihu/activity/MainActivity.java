package com.example.zhihu.activity;

import android.app.Activity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.zhihu.R;
import com.example.zhihu.adapter.NewsAdapter;
import com.example.zhihu.db.DailyNewsDB;
import com.example.zhihu.entity.News;
import com.example.zhihu.task.LoadNewsDetailTask;
import com.example.zhihu.task.LoadNewsFromDBTask;
import com.example.zhihu.task.LoadNewsTask;
import com.example.zhihu.task.listener.onFinishedListener;
import com.example.zhihu.utility.UIHelper;
import com.example.zhihu.utility.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.SimpleFormatter;


public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener {

    private SwipeRefreshLayout mRefreshLayout;
    private ListView mNewsList;

    private NewsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(getDate());

        adapter = new NewsAdapter(this, R.layout.listview_item);

        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        mRefreshLayout.setOnRefreshListener(this);

        mNewsList = (ListView) findViewById(R.id.lv);
        mNewsList.setAdapter(adapter);
        mNewsList.setOnItemClickListener(this);

        if (Utility.checkNetworkConnection()) {
            new LoadNewsFromDBTask(adapter).execute();
        } else {
            Utility.noNetworkAlert();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UIHelper.startNewsDetail(this, adapter.getItem(position));
    }

    @Override
    public void onRefresh() {
        if(Utility.checkNetworkConnection()){
            new LoadNewsTask(adapter, new onFinishedListener() {
                @Override
                public void afterTaskFinished() {
                    mRefreshLayout.setRefreshing(false);
                }
            }).execute();
        } else {
            Utility.noNetworkAlert();
            mRefreshLayout.setRefreshing(false);
        }
    }

    private String getDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("EEE, MMM d, yyyy");
        return sf.format(c.getTime());
    }
}
