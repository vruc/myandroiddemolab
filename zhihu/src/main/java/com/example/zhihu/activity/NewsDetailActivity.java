package com.example.zhihu.activity;

import android.app.Activity;
import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.example.zhihu.R;
import com.example.zhihu.entity.News;
import com.example.zhihu.task.LoadNewsDetailTask;
import com.example.zhihu.utility.Utility;

import java.util.zip.Inflater;

/**
 * Created by ChanRuiDa on 15/6/15.
 */
public class NewsDetailActivity extends Activity {

    private News news;
    private WebView mWebView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
//        getActionBar().setDisplayHomeAsUpEnabled(true);

        mWebView = (WebView) findViewById(R.id.webview);

        setWebView(mWebView);

        Intent i = getIntent();

        News news = (News) i.getSerializableExtra("news");

        if(news == null){
            Utility.showToast("NOT ALLOW");
            return;
        }

//        setTitle(news.getTitle());

        new LoadNewsDetailTask(mWebView).execute(news.getId());

    }

    private void setWebView(WebView mWebView) {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
    }

}
