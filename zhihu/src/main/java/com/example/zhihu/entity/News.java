package com.example.zhihu.entity;

import java.io.Serializable;

/**
 * Created by Oscar on 15/6/11.
 */
public class News implements Serializable {
    private int id;
    private String title;
    private String image;
    private String date;

    public News(){

    }

    public News(int id, String title, String image){
        this.id = id;
        this.title = title;
        this.image = image;
    }

    public News(int id, String title, String image, String date){
        this(id, title, image);
        this.setDate(date);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
