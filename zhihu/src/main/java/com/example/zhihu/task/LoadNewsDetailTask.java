package com.example.zhihu.task;

import android.os.AsyncTask;
import android.webkit.WebView;

import com.example.zhihu.adapter.NewsAdapter;
import com.example.zhihu.entity.News;
import com.example.zhihu.entity.NewsDetail;
import com.example.zhihu.http.Http;
import com.example.zhihu.http.JsonHelper;

import java.util.List;

/**
 * Created by Oscar on 15/6/11.
 */
public class LoadNewsDetailTask extends AsyncTask<Integer, Void, NewsDetail> {

    private WebView mWebView;

    public LoadNewsDetailTask(WebView webView){
        this.mWebView = webView;
    }

    @Override
    protected NewsDetail doInBackground(Integer... params) {
        NewsDetail newsDetail = null;
        try{
            newsDetail = JsonHelper.parseJsonToDetail( Http.getNewsDetail( params[0] ));
        }  finally {
            return newsDetail;
        }
    }

    @Override
    protected void onPostExecute(NewsDetail newsDetail) {
        String headerImage;
        if(newsDetail.getImage() == null || newsDetail.getImage() ==""){
            headerImage = "";
        } else{
            headerImage = newsDetail.getImage();
        }

        StringBuilder sb = new StringBuilder();
        sb.append("<div class=\"img-wrap\">")
                .append("<h1 class=\"headline-title\">")
                .append(newsDetail.getTitle()).append("</h1>")
                .append("<span class=\"img-source\">")
                .append(newsDetail.getImage_source()).append("</span>")
                .append("<img src=\"").append(headerImage)
                .append("\" alt=\"\">")
                .append("<div class=\"img-mask\"></div>");
        String mNewsContent = "<link rel=\"stylesheet\" type=\"text/css\" href=\"news_content_style.css\"/>"
                + "<link rel=\"stylesheet\" type=\"text/css\" href=\"news_header_style.css\"/>"
                + newsDetail.getBody().replace("<div class=\"img-place-holder\">", sb.toString());
        mWebView.loadDataWithBaseURL("file:///android_asset/", mNewsContent, "text/html", "UTF-8", null);
    }
}
