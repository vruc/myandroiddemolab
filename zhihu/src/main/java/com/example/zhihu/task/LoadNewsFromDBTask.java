package com.example.zhihu.task;

import android.os.AsyncTask;

import com.example.zhihu.ZhiHuDailyApplication;
import com.example.zhihu.adapter.NewsAdapter;
import com.example.zhihu.db.DailyNewsDB;
import com.example.zhihu.entity.News;
import com.example.zhihu.http.Http;
import com.example.zhihu.http.JsonHelper;
import com.example.zhihu.task.listener.onFinishedListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Oscar on 15/6/11.
 */
public class LoadNewsFromDBTask extends AsyncTask<Void, Void, List<News>> {

    private NewsAdapter adapter;
    private onFinishedListener listener;

    public LoadNewsFromDBTask(NewsAdapter adapter){
        super();
        this.adapter = adapter;
    }

    public LoadNewsFromDBTask(NewsAdapter adapter, onFinishedListener listener){
        super();
        this.adapter = adapter;
        this.listener = listener;
    }

    @Override
    protected List<News> doInBackground(Void... params) {
        List<News> newsList = null;
        try{
            String date = getDate();
            newsList = DailyNewsDB.getInstance(ZhiHuDailyApplication.getContext()).loadNewsByDate(date);
        } finally {
            return newsList;
        }
    }

    private String getDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
        return sf.format(c.getTime());
    }

    @Override
    protected void onPostExecute(List<News> newsList) {
        adapter.refreshNewsList(newsList);
        if(listener != null){
            listener.afterTaskFinished();
            new LoadNewsTask(adapter, listener);
        } else{
            new LoadNewsTask(adapter);
        }
    }


}
