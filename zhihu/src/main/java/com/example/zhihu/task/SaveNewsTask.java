package com.example.zhihu.task;

import android.os.AsyncTask;

import com.example.zhihu.ZhiHuDailyApplication;
import com.example.zhihu.db.DailyNewsDB;
import com.example.zhihu.entity.News;

import java.util.List;

/**
 * Created by ChanRuiDa on 15/6/15.
 */
public class SaveNewsTask extends AsyncTask<Void, Void, Void> {

    private List<News> newsList;

    public SaveNewsTask(List<News> newsList){
        this.newsList = newsList;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        DailyNewsDB db = DailyNewsDB.getInstance(ZhiHuDailyApplication.getContext());
        for(News news : newsList){
            if(!db.isNewsSaved(news)){
                db.saveNews(news);
            }
        }
        return null;
    }

}
