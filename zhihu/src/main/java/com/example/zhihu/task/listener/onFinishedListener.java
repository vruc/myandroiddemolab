package com.example.zhihu.task.listener;

/**
 * Created by ChanRuiDa on 15/6/15.
 */
public interface onFinishedListener{
    void afterTaskFinished();
}