package com.example.zhihu.task;

import android.os.AsyncTask;
import android.util.Log;

import com.example.zhihu.ZhiHuDailyApplication;
import com.example.zhihu.adapter.NewsAdapter;
import com.example.zhihu.db.DailyNewsDB;
import com.example.zhihu.entity.News;
import com.example.zhihu.http.Http;
import com.example.zhihu.http.JsonHelper;
import com.example.zhihu.task.listener.onFinishedListener;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

/**
 * Created by Oscar on 15/6/11.
 */
public class LoadNewsTask extends AsyncTask<Void, Void, List<News>> {

    private NewsAdapter adapter;
    private onFinishedListener listener;

    public LoadNewsTask(NewsAdapter adapter){
        super();
        this.adapter = adapter;
    }

    public LoadNewsTask(NewsAdapter adapter, onFinishedListener listener){
        super();
        this.adapter = adapter;
        this.listener = listener;
    }

    @Override
    protected List<News> doInBackground(Void... params) {
        List<News> newsList = null;
        try{
            String newInJson = Http.getNewslistLatest();
            newsList = JsonHelper.parseJsonToList(newInJson);
        } finally {
            return newsList;
        }
    }

    @Override
    protected void onPostExecute(List<News> newsList) {
        adapter.refreshNewsList(newsList);
        if(listener != null){
            listener.afterTaskFinished();
        }
        new SaveNewsTask(newsList).execute();
    }
}
