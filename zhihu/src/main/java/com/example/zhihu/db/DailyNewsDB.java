package com.example.zhihu.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;

import com.example.zhihu.entity.News;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oscar on 15/6/11.
 */
public class DailyNewsDB {

    private DBHelper dbHelper;
    private SQLiteDatabase db;

    private static DailyNewsDB mDailyNewsDB;

    private String[] allColmns = {DBHelper.COLUMN_ID, DBHelper.COLUMN_NEWS_ID, DBHelper.COLUMN_NEWS_DATE, DBHelper.COLUMN_NEWS_TITLE, DBHelper.COLUMN_NEWS_IMAGE};

    public DailyNewsDB(Context context){
        dbHelper = new DBHelper(context);
        db =dbHelper.getWritableDatabase();
    }

    public synchronized static DailyNewsDB getInstance(Context context){
        if(mDailyNewsDB == null) {
            mDailyNewsDB = new DailyNewsDB(context);
        }
        return mDailyNewsDB;
    }

    public void saveNews(News news){
        if(news == null) return;

        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_NEWS_ID, news.getId());
        values.put(DBHelper.COLUMN_NEWS_DATE, news.getDate());
        values.put(DBHelper.COLUMN_NEWS_TITLE, news.getTitle());
        values.put(DBHelper.COLUMN_NEWS_IMAGE, news.getImage());

        db.insert( DBHelper.TABLE_DAILY_NEWS, null, values);
    }

    public Boolean isNewsSaved(News news){
        Cursor cursor = db.query(DBHelper.TABLE_DAILY_NEWS, null, DBHelper.COLUMN_NEWS_ID + " = ?", new String[]{ news.getId()+"" }, null, null, null);
        if(cursor.moveToNext()){
            cursor.close();
            return true;
        } else{
            return false;
        }
    }

    public List<News> loadNewsByDate(String date) {
        List<News> newsList = new ArrayList<>();

        Cursor cursor = db.query(DBHelper.TABLE_DAILY_NEWS, null, DBHelper.COLUMN_NEWS_DATE + "= ?", new String[]{ date }, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                News news = new News();
                news.setId(cursor.getInt(1));
                news.setDate(cursor.getString(2));
                news.setTitle(cursor.getString(3));
                news.setImage(cursor.getString(4));
                newsList.add(news);
            }
            while (cursor.moveToNext());
            cursor.close();
        }
        return newsList;
    }

    public void saveFavourite(News news){
        if(news == null) return;

        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_NEWS_ID, news.getId());

        db.insert( DBHelper.TABLE_DAILY_NEWS_FAV, null, values);
    }

    public List<News> loadFavourites(){
        List<News> allNews = new ArrayList<News>();

        Cursor cursor = db.query(DBHelper.TABLE_DAILY_NEWS, null, null, null, null, null, null);

        if(cursor.moveToFirst()){
            do{
//                News news = new News();
//                news.setId( cursor.getInt(1) );
//                news.setTitle(cursor.getString(2));
//                news.setImage(cursor.getString(3));
//                news.setImage(cursor.getString(3));
//                allNews.add(news);
            }while(cursor.moveToNext());
        }
        cursor.close();
        return allNews;
    }

    public Boolean isFavourite(News news){
        Cursor cursor = db.query(DBHelper.TABLE_DAILY_NEWS_FAV, null, DBHelper.COLUMN_NEWS_ID + " = ?", new String[]{ news.getId()+"" }, null, null, null);
        if(cursor.moveToNext()){
            cursor.close();
            return true;
        } else{
            return false;
        }
    }

    public void removeFavourite(News news) {
        if (news == null) return;
        db.delete(DBHelper.TABLE_DAILY_NEWS_FAV, DBHelper.COLUMN_NEWS_ID + " = ?", new String[]{news.getId() + ""});
    }

    public synchronized void closeDB() {
        if (mDailyNewsDB != null) {
            db.close();
        }
    }

}
