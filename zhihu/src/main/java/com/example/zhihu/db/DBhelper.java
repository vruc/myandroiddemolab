package com.example.zhihu.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Oscar on 15/6/11.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "daily_news.db";

    public static final String TABLE_DAILY_NEWS = "daily_news";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NEWS_ID = "news_id";
    public static final String COLUMN_NEWS_DATE = "date";
    public static final String COLUMN_NEWS_TITLE = "news_title";
    public static final String COLUMN_NEWS_IMAGE = "news_image";

    public static final String TABLE_DAILY_NEWS_FAV = "daily_news_fav";

    public static final String TABLE_CREATE_DAILY_NEWS = "CREATE TABLE " + TABLE_DAILY_NEWS
            + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_NEWS_ID + " INTEGER UNIQUE,"
            + COLUMN_NEWS_DATE + " TEXT,"
            + COLUMN_NEWS_TITLE + " TEXT,"
            + COLUMN_NEWS_IMAGE + " TEXT"
            + ");";

    public static final String TABLE_CREATE_DAILY_NEWS_FAV = "CREATE TABLE " + TABLE_DAILY_NEWS_FAV
            + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_NEWS_ID + " INTEGER UNIQUE"
            + ");";

    public static final String TABLE_DROP_DAILY_NEWS = "DROP TABLE IF EXISTS " + TABLE_DAILY_NEWS + ";";
    public static final String TABLE_DROP_DAILY_NEWS_FAV = "DROP TABLE IF EXISTS " + TABLE_DAILY_NEWS_FAV + ";";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE_DAILY_NEWS);
        db.execSQL(TABLE_CREATE_DAILY_NEWS_FAV);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TABLE_DROP_DAILY_NEWS);
        db.execSQL(TABLE_DROP_DAILY_NEWS_FAV);
        onCreate(db);
    }
}
