package com.example.zhihu.http;

import com.example.zhihu.entity.News;
import com.example.zhihu.entity.NewsDetail;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oscar on 15/6/11.
 */
public class JsonHelper {

    public static List<News> parseJsonToList(String json) throws JSONException {

        List<News> newsList = new ArrayList<>();

        JSONObject newsContent = new JSONObject(json);
        JSONArray newsArray = newsContent.getJSONArray("stories");
        String date = newsContent.getString("date");

        for (int i = 0, l = newsArray.length(); i < l; i++) {
            JSONObject newsInJson = newsArray.getJSONObject(i);
            int id = newsInJson.optInt("id");
            String title = newsInJson.optString("title");
            String image = "";
            if(newsInJson.has("images")){
                image = newsInJson.getJSONArray("images").optString(0);
            }

            News news = new News(id, title, image, date);
            newsList.add(news);
        }

        return newsList;
    }

    public static NewsDetail parseJsonToDetail(String json) throws JSONException {
        Gson gson = new Gson();
        return gson.fromJson(json, NewsDetail.class);
    }

}
