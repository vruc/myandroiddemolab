package com.example.zhihu.http;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Oscar on 15/6/11.
 */
public class Http {

    // https://github.com/izzyleung/ZhihuDailyPurify/wiki/%E7%9F%A5%E4%B9%8E%E6%97%A5%E6%8A%A5-API-%E5%88%86%E6%9E%90

    public static final String NEWSLIST_LATEST = "http://news-at.zhihu.com/api/4/news/latest";
    public static final String STORY_VIEW = "http://daily.zhihu.com/story/";
    public static final String NEWS_DETAIL = "http://news-at.zhihu.com/api/4/news/";

    private static final String TAG = Http.class.getSimpleName();

    public static String get(String urlAddr) throws IOException{
        HttpURLConnection connection = null;
        Log.w(TAG, urlAddr);
        try{
            URL url = new URL(urlAddr);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0");

            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                BufferedReader in  = new BufferedReader( new InputStreamReader( connection.getInputStream() ));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while((inputLine = in.readLine())!= null){
                    sb.append(inputLine);
                }
                in.close();
                String content = sb.toString();
                Log.w(TAG, content);
                return content;
            } else{
                throw new IOException("Network Error - response code: " + connection.getResponseCode());
            }
        }
        finally {
            if(connection!=null){
                connection.disconnect();
            }
        }
    }

    public static String getNewslistLatest() throws IOException{
        return get(NEWSLIST_LATEST);
    }

    public static String getNewsDetail(int id) throws IOException
    {
        return get(NEWS_DETAIL + id);
    }
}
