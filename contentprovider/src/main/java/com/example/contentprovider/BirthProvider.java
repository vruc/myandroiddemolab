package com.example.contentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by oscar on 12/3/15.
 */
public class BirthProvider extends ContentProvider
{

    public static final String PROVIDER_NAME = "com.example.contentprovider.Birthday";
    public static final String URL = "content://" + PROVIDER_NAME + "/friends";
    public static final Uri CONTENT_URI = Uri.parse(URL);


    static final String ID = "id";
    static final String NAME = "name";
    static final String BIRTHDAY = "birthday";


    static final int FRIENDS = 1;
    static final int FRIENDS_ID = 2;

    private static HashMap<String, String> BirthMap;

    private static final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "friends", FRIENDS);
        uriMatcher.addURI(PROVIDER_NAME, "friends/#", FRIENDS_ID);
    }

    static final  int DATABASE_VERSION = 1;
    static final  String DATABASE_NAME = "BirthdayReminder";
    static final  String TABLE_NAME = "birthTable";
    static final  String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
            "( id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "  name TEXT NOT NULL," +
            "  birthday TEXT NOT NULL)";

    static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    DBHelper dbHelper;
    SQLiteDatabase database;

    private static class DBHelper extends SQLiteOpenHelper{


        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(DBHelper.class.getSimpleName(),
                    "Upgrading database from version " + oldVersion + " to " + newVersion);
            db.execSQL(DROP_TABLE);
            onCreate(db);
        }
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        dbHelper = new DBHelper(context);
        database = dbHelper.getWritableDatabase();
        return database == null ? false : true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection,
                        String selection, String[] selectionArgs,
                        String sortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_NAME);

        switch (uriMatcher.match(uri)){
            case FRIENDS:
                queryBuilder.setProjectionMap(BirthMap);
                break;
            case FRIENDS_ID:
                queryBuilder.appendWhere(ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        if(sortOrder == null || sortOrder == ""){
            sortOrder = NAME;
        }

        Cursor cursor = queryBuilder.query(
                database, projection,
                selection, selectionArgs,
                null, null,
                sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){
            case FRIENDS:
                return "vnd.android.cursor.dir/vnd.example.friends";
            case FRIENDS_ID:
                return "vnd.android.cursor.items/vnd.example.friends";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        long row = database.insert(TABLE_NAME, "", values);

        if(row > 0){
            Uri newUri = ContentUris.withAppendedId(CONTENT_URI, row);
            getContext().getContentResolver();
            return newUri;
        }
        throw new SQLException("Fail to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)){
            case FRIENDS:
                count = database.delete(TABLE_NAME, selection, selectionArgs);
                break;
            case FRIENDS_ID:
                count = database.delete(TABLE_NAME,
                        ID + " = " +uri.getLastPathSegment() +
                                (!TextUtils.isEmpty(selection) ?
                                        " AND (" + selection + ")"
                                        : ""),
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri );
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)){
            case FRIENDS:
                count = database.update(TABLE_NAME, values, selection, selectionArgs);
                break;
            case FRIENDS_ID:
                count = database.update(TABLE_NAME, values,
                        ID + " = " +uri.getLastPathSegment() +
                                (!TextUtils.isEmpty(selection) ?
                                        " AND (" + selection + ")"
                                        : ""),
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri );
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
