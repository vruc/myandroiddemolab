package com.example.bluetoothchat;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


/**
 * Created by Oscar.Chen on 11/26/2015.
 */
public class BluetoothChatService {

    // Debugging
    private static final String TAG = "BluetoothChatService";
    private static final boolean D = true;

    // Name for the SDP record when creating server socket
    private static final String NAME_SECURE = "BluetoothChatSecure";
    private static final String NAME_INSECURE = "BluetoothChatInsecure";

    private int mState;

    // Unique UUID for this application
    private static final UUID MY_UUID_SECURE   = UUID.fromString("00000000-0000-0000-0000-000000000000");
    private static final UUID MY_UUID_INSECURE = UUID.fromString("11111111-1111-1111-1111-111111111111");


    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device

    private final Handler mHandler;

    public BluetoothChatService(Context context, Handler handler){
        mHandler = handler;
        mState = STATE_NONE;
    }

    public int getState(){
        return mState;
    }

    public void start(){

    }

    public void stop(){

    }

    public void connect(BluetoothDevice device, boolean secure){

    }

    private void setState(int state){

    }

    public void write(byte[] buffer){

    }
}





















