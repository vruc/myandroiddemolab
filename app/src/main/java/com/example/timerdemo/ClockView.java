package com.example.timerdemo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.Calendar;

/**
 * Created by ChanRuiDa on 15/6/3.
 */
public class ClockView extends View {

    private final String TAG = ClockView.class.getSimpleName();

    private Context mContext;

    private int mScreenWidth;
    private int mScreenHeight;

    private Paint mHourPaint;
    private Paint mMinutePaint;
    private Paint mSecondPaint;

    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public ClockView(Context context) {
        super(context);
        this.mContext = context;
        init(context);
    }

    public ClockView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init(context);
    }

    public ClockView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init(context);
    }

    private void init(Context context){
        mHourPaint = new Paint();
        mHourPaint.setColor(Color.WHITE);
        mHourPaint.setAlpha(50);
        mHourPaint.setStyle(Paint.Style.STROKE);
        mHourPaint.setAntiAlias(true);
        mHourPaint.setStrokeWidth(8);

        mMinutePaint = new Paint();
        mMinutePaint.setColor(Color.BLACK);
        mMinutePaint.setStyle(Paint.Style.STROKE);
        mMinutePaint.setAntiAlias(true);
        mMinutePaint.setStrokeWidth(8);

        mSecondPaint = new Paint();
        mSecondPaint.setColor(Color.BLACK);
        mSecondPaint.setStyle(Paint.Style.STROKE);
        mSecondPaint.setAntiAlias(true);
        mSecondPaint.setStrokeWidth(8);

        mHandler = new Handler();
        mHandler.postDelayed(mRunnable, 0);
    }

    private Handler mHandler;
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            invalidate();
            mHandler.postDelayed(mRunnable, 1000);
        }
    };

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = getWidth();
        int height = getHeight();

        Log.w(TAG, width + ", " + height);

        int radius = width / 2 - 10;
        int cx = width / 2;
        int cy = width / 2;

        int x = cx, y = cy, r = radius;

        canvas.drawCircle(cx, cy, radius, mHourPaint);
        canvas.drawCircle(cx, cy, 4, mHourPaint);

        Calendar cal = Calendar.getInstance();

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);
        int milli = cal.get(Calendar.MILLISECOND);

        Log.w(TAG, hour + "," + minute + "," + second + "," + milli + "");

        for(int i=0; i<60; i++) {
            canvas.drawLine(x, y, (float) (x + r * Math.cos(Math.toRadians((i / 60.0f * 360.0f) - 90f))), (float) (y + r * Math.sin(Math.toRadians((i / 60.0f * 360.0f) - 90f))), mPaint);
        }

        float sec=(float)second;
        float min=(float)minute;

        mPaint.setColor(0xFFFF0000);
        canvas.drawLine(x, y, (float)(x+(r-15)*Math.cos(Math.toRadians((hour / 12.0f * 360.0f)-90f))), (float)(y+(r-10)*Math.sin(Math.toRadians((hour / 12.0f * 360.0f)-90f))), mPaint);
        canvas.save();
        mPaint.setColor(0xFF0000FF);
        canvas.drawLine(x, y, (float)(x+r*Math.cos(Math.toRadians((min / 60.0f * 360.0f)-90f))), (float)(y+r*Math.sin(Math.toRadians((min / 60.0f * 360.0f)-90f))), mPaint);
        canvas.save();
        mPaint.setColor(0xFFA2BC13);
        canvas.drawLine(x, y, (float)(x+(r+10)*Math.cos(Math.toRadians((sec / 60.0f * 360.0f)-90f))), (float)(y+(r+15)*Math.sin(Math.toRadians((sec / 60.0f * 360.0f)-90f))), mPaint);

    }
}

