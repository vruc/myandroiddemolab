package com.example.timerdemo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setContentView(new XfermodesView(this));
//        setContentView(new ChangeColorIconWithTextView(this));

        setContentView(R.layout.layout);
//        init();

    }

    ChangeColorIconWithTextView cct1;
    ChangeColorIconWithTextView cct2;
    ChangeColorIconWithTextView cct3;
    ChangeColorIconWithTextView cct4;

    private android.os.Handler mHandler;

    float mAlpha = 0.0f;
    float mStep = 0.05f;

    Runnable ChangeAlpha = new Runnable() {
        @Override
        public void run() {

            mAlpha += mStep;

            cct1.setIconAlpha(mAlpha);
            cct2.setIconAlpha(mAlpha);
            cct3.setIconAlpha(mAlpha);
            cct4.setIconAlpha(mAlpha);

            mHandler.postDelayed(ChangeAlpha, 100);
        }
    };

//    void init(){
//
//        mHandler = new Handler();
//
//        cct1 = (ChangeColorIconWithTextView) findViewById(R.id.id_indicator_one);
//        cct2 = (ChangeColorIconWithTextView) findViewById(R.id.id_indicator_two);
//        cct3 = (ChangeColorIconWithTextView) findViewById(R.id.id_indicator_three);
//        cct4 = (ChangeColorIconWithTextView) findViewById(R.id.id_indicator_four);
//
//        cct1.setIconAlpha(0f);
//
//        cct2.setIconAlpha(0.6f);
//        cct3.setIconAlpha(0.3f);
//        cct4.setIconAlpha(0.7f);
//
//        mHandler.postDelayed(ChangeAlpha, 100);
//
//    }

    @Override
    public void onClick(View v) {

    }

}
