package com.example.timerdemo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Oscar.Chen on 10/14/2015.
 */
public class XfermodesView extends View {

    private Paint mBluePaint;
    private Paint mRedPaint;

    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Paint mPaint;

    private int mColor = 0xFF45C01A;
    private float mAlpha = 0.5f;

    private Bitmap mIconBitmap;
    private Rect mIconRect;

    public XfermodesView(Context ctx){
        super(ctx);
        init();
    }

    public XfermodesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    void init(){
        mBluePaint = new Paint();
        mBluePaint.setColor(0xFFFF0000);

        mRedPaint = new Paint();
        mRedPaint.setColor(0x7F0000FF);

        mPaint= new Paint();
        mPaint.setColor(0xFF45C01A);

        BitmapDrawable drawable = (BitmapDrawable) getResources().getDrawable(R.drawable.btn_rating_star_on_mtrl_alpha);

        mIconBitmap = drawable.getBitmap();
        mIconRect = new Rect(0,0, mIconBitmap.getWidth(), mIconBitmap.getHeight());

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int alpha = (int) Math.ceil((255 * mAlpha));
        canvas.drawBitmap(mIconBitmap, null, mIconRect, null);

//        setupTargetBitmap(alpha);
//        canvas.drawBitmap(mBitmap, 0, 0, null);

    }

    private void setupTargetBitmap(int alpha)
    {
        mBitmap = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        mPaint = new Paint();
        mPaint.setColor(mColor);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setAlpha(alpha);
        mCanvas.drawRect(mIconRect, mPaint);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        mPaint.setAlpha(255);
        mCanvas.drawBitmap(mIconBitmap, null, mIconRect, mPaint);
    }

    private void drawRects(Canvas canvas, PorterDuff.Mode mode){

//            canvas.drawRect(0.0f,0.0f,50.0f,50.0f, mBluePaint);
//            canvas.save();
//
//            canvas.translate(25.0f, 25.0f);
//            mRedPaint.setXfermode(new PorterDuffXfermode(mode));
//            canvas.drawRect(0.0f, 0.0f, 50.0f, 50.0f, mRedPaint);
//            canvas.restore();

    }
}
