package com.example.timerdemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by ChanRuiDa on 15/6/2.
 */
public class MainController extends Activity {

    private WaterWaveView mWaterWaveView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_controller);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mLargeText = (TextView) findViewById(R.id.textView);
        mWaterWaveView = (WaterWaveView) findViewById(R.id.wave_view);
    }

    private int mCurrentVal = 0;
    private Handler mHandler;
    private ProgressBar mProgressBar;
    private TextView mLargeText;

    @Override
    protected void onResume() {
        super.onResume();
        startHandler();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopHandler();
    }

    private int mInterval = 50;

    private void startHandler(){
        mHandler = new Handler();
        mHandler.postDelayed(mRunnable, mInterval);
        mWaterWaveView.startWave();
    }

    private void stopHandler(){
        if(mHandler != null){
            mHandler.removeCallbacks(mRunnable);
            mWaterWaveView.stopWave();
        }
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mProgressBar.setProgress(++mCurrentVal % 100);
//            mLargeText.setTextSize(mCurrentVal % 100);
            mHandler.postDelayed(mRunnable, mInterval);
        }
    };
}
