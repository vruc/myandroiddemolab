package com.example.youhui.views;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
//import android.support.v7.widget.SearchView;
import android.widget.SearchView;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;

import com.example.youhui.R;
import com.example.youhui.adapter.CouponAdapter;
import com.example.youhui.api.GsonHelper;
import com.example.youhui.api.OkHttpClient;
import com.example.youhui.beans.BillList;
import com.example.youhui.beans.Coupon;
import com.example.youhui.util.PreferencesEnum;
import com.example.youhui.util.PreferencesManager;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        initPreference();

        test();
    }

    private void init() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                test();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        lvCoupons = (ListView) findViewById(R.id.lvCoupons);
        gvCoupons = (GridView) findViewById(R.id.gvCoupons);
    }

    private void initPreference() {
        PreferencesManager.initializeInstance(this);
    }

    private ListView lvCoupons;
    private GridView gvCoupons;

    private String TAG = "okHttp";
    private String URL = "https://youhui.95516.com/wm-non-biz-web/restlet/payBill/allBillList?cityCd=440100&isSeckill=&bizAreaCd=&firstTypeCd=&secondTypeCd=&orderType=4&cardIssusers=&cardLevel=&currentPage=1&pageSize=100&version=1.0&source=1";

    private void test() {

        try {
            lvCoupons.setAdapter(null);
            Thread.sleep(1000);
            OkHttpClient.get(URL, new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    Log.e("okHttp", e.getMessage());
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    String body = response.body().string();

                    Log.d(TAG, response.code() + "");
                    Log.d(TAG, body);

                    final BillList list = GsonHelper.fromJsonObject(BillList.class, body);

                    Log.d(TAG, list.getCurrentPage() + "");
                    Log.d(TAG, list.getPageSize() + "");
                    Log.d(TAG, list.getTotalNum() + "");
                    Log.d(TAG, list.getData().size() + "");

//                    final List<String> adapterData = new ArrayList<String>();

//                    int idx = 1;
//
//                    ArrayList<Coupon> coupons = new ArrayList<Coupon>();
//                    for (Coupon coupon : list.getData()) {
//                        String str = idx + " " + coupon.getTicketNm() + "," + coupon.getActivityDesc();
//                        adapterData.add(str);
//                        idx++;
//                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            lvCoupons.setAdapter(new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, adapterData));
//                            lvCoupons.setAdapter(new CouponAdapter(MainActivity.this, R.layout.list_coupon, list.getData()));
                            gvCoupons.setAdapter(new CouponAdapter(MainActivity.this, R.layout.grid_coupon, list.getData()));
                        }
                    });


                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        // Assumes current activity is the searchable activity
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String str = "";
        if (id == R.id.nav_camara) {
            // Handle the camera action
            str = "nav_camara";
        } else if (id == R.id.nav_gallery) {
            str = "nav_gallery";
        } else if (id == R.id.nav_slideshow) {
            str = "nav_slideshow";
        } else if (id == R.id.nav_manage) {
            str = "nav_manage";
        } else if (id == R.id.nav_share) {
            str = "nav_share";
        } else if (id == R.id.nav_send) {
            str = "nav_send";
        }

        Log.d("drawer", str);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}