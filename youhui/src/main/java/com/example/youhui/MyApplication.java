package com.example.youhui;

import android.app.Application;
import android.content.Context;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

/**
 * Created by Oscar.Chen on 10/13/2015.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initImageLoader(this);
    }

    private void initImageLoader(Context context){

        File cacheDir = StorageUtils.getOwnCacheDirectory(context, "ImageCache");

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024);
        config.discCache(new UnlimitedDiscCache(cacheDir));
        config.tasksProcessingOrder(QueueProcessingType.LIFO);

        if(!"PROD".equals(BuildConfig.FLAVOR)){
            config.writeDebugLogs();
        }

        ImageLoader.getInstance().init(config.build());
    }
}
