package com.example.youhui.api;


import com.squareup.okhttp.Request;

import java.io.IOException;

/**
 * Created by Oscar.Chen on 10/12/2015.
 */
public class OkHttpClient {

    private static final com.squareup.okhttp.OkHttpClient mOkHttpClient = new com.squareup.okhttp.OkHttpClient();

    public static void get(String url, com.squareup.okhttp.Callback callback) throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .build();

        mOkHttpClient.newCall(request).enqueue(callback);
    }

}
