package com.example.youhui.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

/**
 * Created by Oscar.Chen on 10/12/2015.
 */
public class GsonHelper{

    private static Gson getInstance() {
        Gson gson =  new GsonBuilder().create();
        return gson;
    }

    public static <T> T fromJsonObject(Class<T> clazz, String jsonObject) {
        if (jsonObject == null)
            return null;
        return getInstance().fromJson(jsonObject, clazz);
    }

    public static String toJsonString(Object object){
        return getInstance().toJson(object);
    }
}
