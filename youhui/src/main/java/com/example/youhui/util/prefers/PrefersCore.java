package com.example.youhui.util.prefers;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.youhui.util.PreferencesManager;

/**
 * Created by Oscar.Chen on 10/13/2015.
 */
public class PrefersCore {

    private static final String PREF_NAME = "Prefers_Core";

    private static PrefersCore sInstance;
    private final SharedPreferences mPref;

    private PrefersCore(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public SharedPreferences getSharedPreferences() {
        return mPref;
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PrefersCore(context);
        }
    }

    public static synchronized PrefersCore getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PreferencesManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }

}
