package com.example.youhui.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.youhui.util.prefers.PrefersCore;
import com.example.youhui.util.prefers.PrefersDefault;

/**
 * Created by Oscar.Chen on 10/13/2015.
 */
public class PreferencesManager {

    private SharedPreferences mPref;

    public PreferencesManager(PreferencesEnum prefs) {
        switch ( prefs) {
            case Core:
                this.mPref = PrefersCore.getInstance().getSharedPreferences();
                break;
            default:
                this.mPref = PrefersDefault.getInstance().getSharedPreferences();
                break;
        }

    }

    public static void initializeInstance(Context context) {
        PrefersCore.initializeInstance(context);
        PrefersDefault.initializeInstance(context);
    }

    public void setString(String key, String value){
        mPref.edit()
                .putString(key, value)
                .commit();
    }

    public void setInt(String key, int value){
        mPref.edit()
                .putInt(key, value)
                .commit();
    }

    public void setBoolean(String key, Boolean value){
        mPref.edit()
                .putBoolean(key, value)
                .commit();
    }

    public String getString(String key){
        return mPref.getString(key, "");
    }

    public int getInt(String key){
        return mPref.getInt(key, 0);
    }

    public boolean getBoolean(String key){
        return mPref.getBoolean(key, false);
    }

    public void remove(String key) {
        mPref.edit()
                .remove(key)
                .commit();
    }

    public boolean clear() {
        return mPref.edit()
                .clear()
                .commit();
    }
}

