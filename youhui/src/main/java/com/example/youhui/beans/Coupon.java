package com.example.youhui.beans;

/**
 * Created by Oscar.Chen on 10/12/2015.
 */
public class Coupon {
    private String billType;
    private String brandNm;
    private String brandId;
    private String activityDesc;
    private String billPicPath;
    private String billId;
    private String brandUrl;
    private String ticketNm;
    private double ticketPrice;
    private Integer branchNum;
    private String updTime;
    private Integer clientChnl;
    private Integer isSeckill;
    private Integer leftNum;
    private String exclusiveTip;
    private Boolean isNew;
    private double price;
    private Integer distance;
    private Integer score;
    private Integer downloadNum;
    private Boolean isExclusive;
    private Boolean canBeDownload;
    private String billEndDt;
    private Boolean couponLeftNumStatus;
    private Boolean couponDateStatus;
    private String downloadBeginDt;
    private String downloadEndDt;
    private String seckillBeginTm;
    private String currentTime;
    private Boolean saleIn;
    private String salePrice;
    private String originPrice;
    private String priceTrend;
    private Integer payTimeout;
    private Boolean autoDrawback;
    private Boolean mustAppointment;
    private Boolean anytimeDrawback;

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getBrandNm() {
        return brandNm;
    }

    public void setBrandNm(String brandNm) {
        this.brandNm = brandNm;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getActivityDesc() {
        return activityDesc;
    }

    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc;
    }

    public String getBillPicPath() {
        return billPicPath;
    }

    public void setBillPicPath(String billPicPath) {
        this.billPicPath = billPicPath;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getBrandUrl() {
        return brandUrl;
    }

    public void setBrandUrl(String brandUrl) {
        this.brandUrl = brandUrl;
    }

    public String getTicketNm() {
        return ticketNm;
    }

    public void setTicketNm(String ticketNm) {
        this.ticketNm = ticketNm;
    }

    public double getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(double  ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public Integer getBranchNum() {
        return branchNum;
    }

    public void setBranchNum(Integer branchNum) {
        this.branchNum = branchNum;
    }

    public String getUpdTime() {
        return updTime;
    }

    public void setUpdTime(String updTime) {
        this.updTime = updTime;
    }

    public Integer getClientChnl() {
        return clientChnl;
    }

    public void setClientChnl(Integer clientChnl) {
        this.clientChnl = clientChnl;
    }

    public Integer getIsSeckill() {
        return isSeckill;
    }

    public void setIsSeckill(Integer isSeckill) {
        this.isSeckill = isSeckill;
    }

    public Integer getLeftNum() {
        return leftNum;
    }

    public void setLeftNum(Integer leftNum) {
        this.leftNum = leftNum;
    }

    public String getExclusiveTip() {
        return exclusiveTip;
    }

    public void setExclusiveTip(String exclusiveTip) {
        this.exclusiveTip = exclusiveTip;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getDownloadNum() {
        return downloadNum;
    }

    public void setDownloadNum(Integer downloadNum) {
        this.downloadNum = downloadNum;
    }

    public Boolean getIsExclusive() {
        return isExclusive;
    }

    public void setIsExclusive(Boolean isExclusive) {
        this.isExclusive = isExclusive;
    }

    public Boolean getCanBeDownload() {
        return canBeDownload;
    }

    public void setCanBeDownload(Boolean canBeDownload) {
        this.canBeDownload = canBeDownload;
    }

    public String getBillEndDt() {
        return billEndDt;
    }

    public void setBillEndDt(String billEndDt) {
        this.billEndDt = billEndDt;
    }

    public Boolean getCouponLeftNumStatus() {
        return couponLeftNumStatus;
    }

    public void setCouponLeftNumStatus(Boolean couponLeftNumStatus) {
        this.couponLeftNumStatus = couponLeftNumStatus;
    }

    public Boolean getCouponDateStatus() {
        return couponDateStatus;
    }

    public void setCouponDateStatus(Boolean couponDateStatus) {
        this.couponDateStatus = couponDateStatus;
    }

    public String getDownloadBeginDt() {
        return downloadBeginDt;
    }

    public void setDownloadBeginDt(String downloadBeginDt) {
        this.downloadBeginDt = downloadBeginDt;
    }

    public String getDownloadEndDt() {
        return downloadEndDt;
    }

    public void setDownloadEndDt(String downloadEndDt) {
        this.downloadEndDt = downloadEndDt;
    }

    public String getSeckillBeginTm() {
        return seckillBeginTm;
    }

    public void setSeckillBeginTm(String seckillBeginTm) {
        this.seckillBeginTm = seckillBeginTm;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public Boolean getSaleIn() {
        return saleIn;
    }

    public void setSaleIn(Boolean saleIn) {
        this.saleIn = saleIn;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(String originPrice) {
        this.originPrice = originPrice;
    }

    public String getPriceTrend() {
        return priceTrend;
    }

    public void setPriceTrend(String priceTrend) {
        this.priceTrend = priceTrend;
    }

    public Integer getPayTimeout() {
        return payTimeout;
    }

    public void setPayTimeout(Integer payTimeout) {
        this.payTimeout = payTimeout;
    }

    public Boolean getAutoDrawback() {
        return autoDrawback;
    }

    public void setAutoDrawback(Boolean autoDrawback) {
        this.autoDrawback = autoDrawback;
    }

    public Boolean getMustAppointment() {
        return mustAppointment;
    }

    public void setMustAppointment(Boolean mustAppointment) {
        this.mustAppointment = mustAppointment;
    }

    public Boolean getAnytimeDrawback() {
        return anytimeDrawback;
    }

    public void setAnytimeDrawback(Boolean anytimeDrawback) {
        this.anytimeDrawback = anytimeDrawback;
    }
}
