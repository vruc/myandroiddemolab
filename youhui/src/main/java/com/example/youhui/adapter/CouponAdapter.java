package com.example.youhui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.youhui.R;
import com.example.youhui.beans.Coupon;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

/**
 * Created by Oscar.Chen on 10/13/2015.
 */
public class CouponAdapter extends ArrayAdapter<Coupon> {

    private int resourceId;

    public CouponAdapter(Context context, int resource, List<Coupon> objects) {
        super(context, resource, objects);
        this.resourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder vh = new ViewHolder();
        View v = convertView;

        if(v == null){
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(resourceId, null);

            vh.brandNm = (TextView) v.findViewById(R.id.tv_brandNm);
            vh.activityDesc = (TextView) v.findViewById(R.id.tv_activityDesc);
            vh.billPicPath = (ImageView) v.findViewById(R.id.iv_billPicPath);

            v.setTag(vh);
        } else {
            vh = (ViewHolder) v.getTag();
        }

        Coupon item = getItem(position);

        vh.brandNm.setText(item.getBrandNm());
        vh.activityDesc.setText(item.getActivityDesc());

//        ImageLoader.getInstance().displayImage("http://youhui.95516.com/"+item.getBillPicPath(), vh.billPicPath);

        final ViewHolder finalVh = vh;
        ImageLoader.getInstance().loadImage("http://youhui.95516.com/"+item.getBillPicPath(), defaultOptions(), new SimpleImageLoadingListener(){
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                finalVh.billPicPath.setImageBitmap(loadedImage);
            }
        });

        return v;
    }

    private class ViewHolder {
        TextView brandNm;
        TextView activityDesc;
        ImageView billPicPath;
    }

    private DisplayImageOptions defaultOptions(){
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        return options;
    }

}
