package com.example.game2048;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by Oscar on 15/7/25.
 */
public class Card extends FrameLayout {

    private TextView mNum;
    private int iNum = 0;

    public Card(Context context) {
        super(context);

        mNum = new TextView(getContext());
        mNum.setTextSize(32);
        mNum.setGravity(Gravity.CENTER);

        LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        lp.setMargins(10, 10, 0, 0);
        mNum.setLayoutParams(lp);

        addView(mNum);
        setNum(0);

    }

    public int getNum() {
        return iNum;
    }

    public void setNum(int number) {
        this.iNum = number;

        if(number == 0){
            mNum.setText("");
        } else {
            mNum.setText(iNum + "");
        }

        mNum.setBackgroundColor(0xff123123);

    }

    public boolean equals(Card card) {
        return this.getNum() == card.getNum();
    }
}
