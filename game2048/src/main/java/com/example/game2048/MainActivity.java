package com.example.game2048;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    private Button mStartNewGame;
    private TextView mScore;
    private int score = 0;

    public MainActivity(){
        mainActivity = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        mScore = (TextView) findViewById(R.id.tvScore);
        mStartNewGame = (Button) findViewById(R.id.btnStartNewGame);

        mStartNewGame.setOnClickListener(this);

    }

    public void clearScore(){
        score = 0;
        mScore.setText(score + "");
    }

    public void addScore(int val){
        score += val;
        mScore.setText(score + "");
    }

    public int getScore(){
        return score;
    }

    private static MainActivity mainActivity;

    public static MainActivity getMainActivity(){
        return mainActivity;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnStartNewGame:

                GameView.getInstance().startNewGame();

                break;
        }
    }
}
